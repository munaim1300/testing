# Use the Alpine base image
FROM alpine:latest

# Update package repository and install git
RUN apk update && apk add --no-cache git

# Set the working directory
WORKDIR /app

# Copy the contents of the current directory into the container at /app
COPY . .

# Command to run when the container starts
CMD ["/bin/sh"]
